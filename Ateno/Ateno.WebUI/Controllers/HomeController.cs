﻿using Ateno.Application.DTOs;
using Ateno.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Ateno.WebUI.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IControllerService _controllerService;
        private readonly IStudyDeckService _studyDeckService;

        public HomeController(IControllerService controllerService, IStudyDeckService studyDeckService)
        {
            _controllerService = controllerService;
            _studyDeckService = studyDeckService;
        }

        public IActionResult Index()
        {
            HomeDTO data = _controllerService.LoadHome(User.FindFirstValue(ClaimTypes.NameIdentifier));
            return View(data);
        }

        public ActionResult Logout()
        {
            return PartialView();
        }

        public ActionResult RemoveStudyDeck(int id)
        {
            try
            {
                StudyDeckDTO studyDeck = _studyDeckService.LoadStudyDeck(id, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (studyDeck != null)
                    return PartialView(studyDeck);
            }
            catch { }
            TempData["warning"] = "Falha ao localizar o Baralho de Estudo.";
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveStudyDeckConfirmed(int id)
        {
            try
            {
                ResponseDTO response = await _studyDeckService.RemoveDeck(id, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if(response.Success)
                    TempData["success"] = "Baralho removido com sucesso!";
                else
                    TempData["warning"] = response.Message;
            }
            catch
            {
                TempData["warning"] = "Falha ao remover Baralho de Estudo.";
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
