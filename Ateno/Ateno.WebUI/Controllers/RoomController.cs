﻿using Ateno.Application.DTOs;
using Ateno.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Ateno.WebUI.Controllers
{
    [Authorize]
    public class RoomController : Controller
    {
        private readonly IControllerService _controllerService;
        private readonly IStudyDeckService _studyDeckService;
        private readonly IRoomService _roomService;

        public RoomController(IControllerService controllerService, IStudyDeckService studyDeckService, IRoomService roomService)
        {
            _controllerService = controllerService;
            _studyDeckService = studyDeckService;
            _roomService = roomService;
        }

        public IActionResult Index(int roomId)
        {
            try
            {
                HomeRoomDTO model = _controllerService.LoadRoom(roomId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (model != null)
                    return View(model);
                TempData["warning"] = "Sala não disponível.";
            }
            catch
            {
                TempData["warning"] = "Ocorreu uma falha ao acessar a Sala.";
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RoomDTO data)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    data.AdminId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                    ResponseDTO response = await _roomService.Create(data);
                    if (response.Success)
                    {
                        TempData["success"] = "Sala criada com sucesso!!";
                        return RedirectToAction("Index", "Home");
                    }
                    TempData["warning"] = response.Message;
                }
            }
            catch
            {
                TempData["warning"] = "Ocorreu uma falha ao criar a Sala.";
            }
            return View(data);
        }

        public IActionResult Edit(int roomId)
        {
            try
            {
                RoomDTO data = _roomService.getRoom(roomId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if(data != null)
                    return View(data);
                TempData["warning"] = "Falha ao carregar as informações da Sala.";
            }
            catch
            {
                TempData["warning"] = "Falha ao carregar as informações da Sala.";
            }
            return RedirectToAction("Index", "Room", new { roomId = roomId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(RoomDTO data)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ResponseDTO response = await _roomService.Edit(data, User.FindFirstValue(ClaimTypes.NameIdentifier));
                    if (response.Success)
                    {
                        TempData["success"] = "Sala editada com sucesso!";
                        return RedirectToAction("Edit", "Room", new { roomId = data.Id });
                    }
                    TempData["warning"] = response.Message;
                }
            }
            catch
            {
                TempData["warning"] = "Ocorreu uma falha ao editar a Sala.";
            }
            return View(data);
        }

        public IActionResult NewRoom()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EnterRoom(string roomCode)
        {
            try
            {
                ResponseDTO response = await _roomService.EnterRoom(roomCode, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (response.Success)
                    TempData["success"] = "Sala de estudos vinculada com sucesso!";
                else
                    TempData["warning"] = response.Message;
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                TempData["warning"] = "Falha ao adicionar Sala de estudos.";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUser(int roomId, string email)
        {
            try
            {
                ResponseDTO response = await _roomService.AddUser(roomId, email, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (response.Success)
                    TempData["success"] = "Usuário adicionado com sucesso!";
                else
                    TempData["warning"] = response.Message;
                return RedirectToAction("Edit", "Room", new { roomId = roomId });
            }
            catch
            {
                TempData["warning"] = "Falha ao adicionar usuário na Sala.";
                return RedirectToAction("Index", "Room");
            }
        }

        public IActionResult RemoveUser(int roomId, string email)
        {
            try
            {
                ViewBag.roomId = roomId;
                ViewBag.email = email;
                return PartialView();
            }
            catch
            {
                TempData["warning"] = "Falha ao buscar usuário na Sala.";
                return RedirectToAction("Index", "Room");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveUserConfirmed(int roomId, string email)
        {
            try
            {
                ResponseDTO response = await _roomService.RemoveUser(roomId, email, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (response.Success)
                    TempData["success"] = "Usuário removido com sucesso!";
                else
                    TempData["warning"] = response.Message;
                return RedirectToAction("Edit", "Room", new { roomId = roomId });
            }
            catch
            {
                TempData["warning"] = "Falha ao remover usuário da Sala.";
                return RedirectToAction("Index", "Room");
            }
        }

        public ActionResult RemoveStudyDeck(int id)
        {
            try
            {
                StudyDeckDTO studyDeck = _studyDeckService.LoadStudyDeck(id, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (studyDeck != null)
                    return PartialView(studyDeck);
            }
            catch { }
            TempData["warning"] = "Falha ao localizar o Baralho de Estudo.";
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveStudyDeckConfirmed(int id)
        {
            try
            {
                ResponseDTO response = await _studyDeckService.RemoveDeck(id, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (response.Success)
                    TempData["success"] = "Baralho removido com sucesso!";
                else
                    TempData["warning"] = response.Message;
                return RedirectToAction("Index", "Room", new { roomId = response.Value });
            }
            catch
            {
                TempData["warning"] = "Falha ao remover Baralho de Estudo.";
                return RedirectToAction("Index", "Room");
            }
        }

        public async Task<ActionResult> DeckInfo(int deckId)
        {
            try
            {
                RoomDeckInfoDTO roomDeckInfo = await _roomService.RoomDeckInfo(deckId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (roomDeckInfo != null)
                    return PartialView(roomDeckInfo);
            }
            catch { }
            TempData["warning"] = "Falha ao localizar o Baralho de Estudo.";
            return RedirectToAction("Index", "Home");
        }

        public IActionResult RemoveRoom(int roomId)
        {
            try
            {
                ViewBag.roomId = roomId;
                return PartialView();
            }
            catch
            {
                TempData["warning"] = "Falha ao buscar usuário na Sala.";
                return RedirectToAction("Index", "Room");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveRoomConfirmed(int roomId)
        {
            try
            {
                ResponseDTO response = await _roomService.Remove(roomId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (response.Success)
                    TempData["success"] = "Sala removida com sucesso!";
                else
                    TempData["warning"] = response.Message;
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                TempData["warning"] = "Falha ao remover Sala.";
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
