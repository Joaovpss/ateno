﻿using Ateno.Application.DTOs;
using Ateno.Application.Interfaces;
using Ateno.WebUI.ViewModels.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Ateno.WebUI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        public IActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (string.IsNullOrEmpty(returnUrl))
                    return RedirectToAction("Index", "Home");
                else
                    return Redirect(returnUrl);
            }
            return View(new LoginViewModel()
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ResponseDTO result = await _userService.Authentication(model.Email, model.Password);

                    if (result.Success)
                    {
                        if (string.IsNullOrEmpty(model.ReturnUrl))
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError("Password", result.Message);
                        return View(model);
                    }
                }
            }
            catch { }
            return View(model);
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel data)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ResponseDTO response = await _userService.Register(data.user, data.Password);
                    if (response.Success)
                        return RedirectToAction("Index", "Home");
                    else
                        ModelState.AddModelError("ConfirmPassword", response.Message);
                }
            }
            catch { }
            return View(data);
        }

        [Authorize]
        public IActionResult ChangeData()
        {
            try
            {
                UserDTO user = _userService.GetById(User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (user != null)
                {
                    ChangeDataViewModel model = new ChangeDataViewModel();
                    model.userData.Name = user.Name;
                    model.userData.Email = user.Email;
                    return View(model);
                }
            }
            catch { }
            TempData["warning"] = "Falha ao carregar as informações do usuário.";
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeData(UserDataViewModel data)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ResponseDTO response = await _userService.Update(data.Name, data.Email, User.FindFirstValue(ClaimTypes.NameIdentifier));
                    if (response.Success)
                    {
                        TempData["success"] = "Os dados foram alterados com sucesso!";
                        return RedirectToAction("Index", "Home");
                    }
                    TempData["warning"] = response.Message;
                }
                else
                    TempData["warning"] = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).FirstOrDefault();
                ChangeDataViewModel model = new ChangeDataViewModel();
                model.userData = data;
                return View(model);
            }
            catch { }
            TempData["warning"] = "Falha ao alterar dados.";
            return RedirectToAction("ChangeData", "Account");
        }

        [Authorize]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel data)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ResponseDTO response = await _userService.ChangePassword(User.FindFirstValue(ClaimTypes.NameIdentifier), data.currentPassword, data.newPassword);
                    if (response.Success)
                    {
                        TempData["success"] = "A Senha foi alterada com sucesso!";
                        return RedirectToAction("Index", "Home");
                    }
                    TempData["warning"] = response.Message;
                }
                else
                    TempData["warning"] = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).FirstOrDefault();
                return RedirectToAction("ChangeData", "Account");
            }
            catch { }
            TempData["warning"] = "Falha ao alterar a senha.";
            return RedirectToAction("ChangeData", "Account");
        }

        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _userService.Logout();
            return RedirectToAction("Login");
        }
    }
}
