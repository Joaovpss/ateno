﻿using Ateno.Application.DTOs;
using Ateno.Application.Interfaces;
using Ateno.Domain.Enum;
using Ateno.WebUI.ViewModels.StudyDeck;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Ateno.WebUI.Controllers
{
    [Authorize]
    public class StudyDeckController : Controller
    {
        private readonly IStudyProcessService _studyProcessService;
        private readonly IStudyDeckService _studyDeckService;
        private readonly IRoomService _roomService;

        public StudyDeckController(IStudyProcessService studyProcessService, IStudyDeckService studyDeckService, IRoomService roomService)
        {
            _studyProcessService = studyProcessService;
            _studyDeckService = studyDeckService;
            _roomService = roomService;
        }

        public IActionResult Index(int deckid, bool sequence = false)
        {
            try
            {
                bool has = _studyProcessService.HasStudy(deckid, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (has)
                {
                    StudyCardDTO studyCardDTO = _studyProcessService.LoadStudy(deckid, User.FindFirstValue(ClaimTypes.NameIdentifier));
                    if (studyCardDTO != null)
                        return View(studyCardDTO);
                }
                else
                {
                    if (sequence)
                        TempData["success"] = "Os estudos deste baralho foram finalizados com sucesso!";
                    else
                        TempData["info"] = "Não há estudos pendentes neste Baralho.";
                    int roomId = _studyDeckService.GetRoomIdByDeck(deckid);
                    if (roomId == 0)
                        return RedirectToAction("Index", "Home");
                    else
                        return RedirectToAction("Index", "Room", new { roomId = roomId });
                }
            }
            catch { }
            TempData["warning"] = "Falha ao carregar Estudo.";
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(int id, int answer)
        {
            try
            {
                ResponseDTO response = await _studyProcessService.SaveStudy(id, answer, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (response.Success)
                    return RedirectToAction("Index", "StudyDeck", new { deckid = response.Value, sequence = true });
                TempData["warning"] = response.Message;
            }
            catch
            {
                TempData["warning"] = "Falha ao realizar Estudo.";
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Create(int? roomId)
        {
            if(roomId != null)
            {
                bool access = _roomService.IsAdmin((int)roomId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (!access)
                {
                    TempData["warning"] = "Sala não encontrada ou sem permissão de administrador.";
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.RoomId = roomId;
            }
            else
                ViewBag.RoomId = -1;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if(model.Deck.StudyRoomId > 0)
                    {
                        bool access = _roomService.IsAdmin(model.Deck.StudyRoomId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                        if (!access)
                        {
                            TempData["warning"] = "Sala não encontrada ou sem permissão de administrador.";
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    if (model.Deck.StudyRoomId == -1)
                        model.Deck.UserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                    ResponseDTO response = await _studyDeckService.Create(model.Deck, model.Cards);
                    if (response.Success)
                    {
                        TempData["success"] = "Baralho cadastrado com sucesso!!";
                        if (model.Deck.StudyRoomId < 1)
                            return RedirectToAction("Index", "Home");
                        else
                            return RedirectToAction("Index", "Room", new { roomId = model.Deck.StudyRoomId });
                    }
                    TempData["warning"] = response.Message;
                }
                return View(model);
            }
            catch
            {
                TempData["warning"] = "Falha ao cadastrar o Baralho de Estudos.";
                return View(model);
            }
        }

        public IActionResult AddCards(int studyDeckId)
        {
            try
            {
                Permission check = _studyDeckService.AccessAllowed(studyDeckId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (check == Permission.Admin)
                {
                    AddCardsViewModel model = new AddCardsViewModel();
                    model.studyDeckId = studyDeckId;
                    return PartialView(model);
                }
            }
            catch { }
            TempData["warning"] = "Falha ao localizar baralho.";
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddCards(AddCardsViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ResponseDTO response = await _studyDeckService.AddCards(model.studyDeckId, model.Cards, User.FindFirstValue(ClaimTypes.NameIdentifier));
                    if (response.Success)
                    {
                        TempData["success"] = "Cartas adicionadas com sucesso!!";
                        if (response.Value == 0)
                            return RedirectToAction("Index", "Home");
                        else
                            return RedirectToAction("Index", "Room", new { roomId = response.Value });
                    }
                    TempData["warning"] = response.Message;
                }
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        public IActionResult EditStudyDeck(int studyDeckId)
        {
            try
            {
                Permission check = _studyDeckService.AccessAllowed(studyDeckId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (check == Permission.Admin)
                {
                    StudyDeckDTO model = _studyDeckService.LoadStudyDeck(studyDeckId, User.FindFirstValue(ClaimTypes.NameIdentifier), true);
                    return View(model);
                }
            }
            catch { }
            TempData["warning"] = "Falha ao localizar baralho.";
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditStudyDeck(int id, string name)
        {
            try
            {
                if (id < 1 | !string.IsNullOrWhiteSpace(name))
                {
                    ResponseDTO response = await _studyDeckService.UpdateName(id, name, User.FindFirstValue(ClaimTypes.NameIdentifier));
                    if (response.Success)
                    {
                        TempData["success"] = "Nome do baralho alterado com sucesso!";
                        return RedirectToAction("EditStudyDeck", "StudyDeck", new { studyDeckId = id });
                    }
                    TempData["warning"] = response.Message;
                    return RedirectToAction("EditStudyDeck", "StudyDeck", new { studyDeckId = id });
                }
                else
                {
                    TempData["alert"] = "O Nome inserido é inválido.";
                    return RedirectToAction("EditStudyDeck", "StudyDeck", new { studyDeckId = id });
                }
            }
            catch { }
            TempData["warning"] = "Falha ao localizar baralho.";
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> DeckInfo(int deckId)
        {
            try
            {
                DeckInfoDTO DeckInfo = await _studyDeckService.DeckInfo(deckId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                if (DeckInfo != null)
                    return PartialView(DeckInfo);
            }
            catch { }
            TempData["warning"] = "Falha ao localizar o Baralho de Estudo.";
            return RedirectToAction("Index", "Home");
        }

        public IActionResult RemoveStudyCard(int id)
        {
            try
            {
                if (id > 0)
                {
                    StudyCardDTO model = _studyDeckService.LoadStudyCard(id, User.FindFirstValue(ClaimTypes.NameIdentifier));
                    if (model != null)
                        return PartialView(model);
                }
            }
            catch { }
            TempData["warning"] = "Falha ao localizar a Carta para remoção.";
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveStudyCard(int id, int studyDeckId)
        {
            try
            {
                if (id > 1)
                {
                    ResponseDTO response = await _studyDeckService.RemoveCard(id, User.FindFirstValue(ClaimTypes.NameIdentifier));
                    if (response.Success)
                    {
                        TempData["success"] = "O cartão foi removido deste baralho!";
                        return RedirectToAction("EditStudyDeck", "StudyDeck", new { studyDeckId = studyDeckId });
                    }
                    TempData["warning"] = response.Message;
                    return RedirectToAction("EditStudyDeck", "StudyDeck", new { studyDeckId = studyDeckId });
                }
            }
            catch { }
            TempData["warning"] = "Falha ao localizar a Carta para remoção.";
            return RedirectToAction("Index", "Home");
        }
    }
}