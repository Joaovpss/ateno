﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ateno.WebUI.ViewModels.Account
{
    public class ChangeDataViewModel
    {
        public ChangeDataViewModel()
        {
            userData = new UserDataViewModel();
            changePassword = new ChangePasswordViewModel();
        }

        public UserDataViewModel userData { get; set; }
        public ChangePasswordViewModel changePassword { get; set; }
    }

    public class UserDataViewModel
    {
        public UserDataViewModel() { }

        [Required(ErrorMessage = "O Nome é obrigatório")]
        [MinLength(5)]
        [MaxLength(128)]
        [DisplayName("Nome Completo")]
        public string Name { get; set; }

        [Required(ErrorMessage = "O Email é obrigatório")]
        [MinLength(7)]
        [MaxLength(128)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }

    public class ChangePasswordViewModel
    {
        public ChangePasswordViewModel() { }

        [Required]
        [Display(Name = "Senha Atual")]
        [DataType(DataType.Password)]
        public string currentPassword { get; set; }

        [Required]
        [Display(Name = "Nova Senha")]
        [DataType(DataType.Password)]
        public string newPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Nova Senha")]
        [Compare("newPassword", ErrorMessage = "As senhas não correspondem.")]
        public string ConfirmNewPassword { get; set; }
    }
}
