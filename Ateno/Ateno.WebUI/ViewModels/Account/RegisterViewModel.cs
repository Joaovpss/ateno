﻿using Ateno.Application.DTOs;
using System.ComponentModel.DataAnnotations;

namespace Ateno.WebUI.ViewModels.Account
{
    public class RegisterViewModel
    {
        public UserDTO user { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Senha")]
        [Compare("Password", ErrorMessage = "As senhas não correspondem.")]
        public string ConfirmPassword { get; set; }
    }
}
