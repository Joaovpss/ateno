﻿$(document).ready(function () {
    $(function () {
        $("main").on("click", ".addCards", function () {
            $("#modalAddCards").load("/StudyDeck/AddCards?studyDeckId=" + $(this).attr("data-id"), function () {
                $("#modalAddCards").modal('show');
            })
        });
    });

    $(function () {
        $("main").on("click", ".removeDeck", function () {
            $("#modalConfirm").load("/Home/RemoveStudyDeck?id=" + $(this).attr("data-id"), function () {
                $("#modalConfirm").modal('show');
            })
        });
    });

    $(function () {
        $("main").on("click", "#newRoom", function () {
            $("#modalConfirm").load("/Room/NewRoom", function () {
                $("#modalConfirm").modal('show');
            })
        });
    });

    $(function () {
        $("main").on("click", ".deckInfo", function () {
            $("#modalInfo").load("/StudyDeck/DeckInfo?deckId=" + $(this).attr("data-id"), function () {
                $("#modalInfo").modal('show');
            })
        });
    });

    $(function () {
        $("main").on("click", ".removeRoom", function () {
            $("#modalConfirm").load("/Room/RemoveRoom?roomId=" + $(this).attr("data-id"), function () {
                $("#modalConfirm").modal('show');
            })
        });
    });
});