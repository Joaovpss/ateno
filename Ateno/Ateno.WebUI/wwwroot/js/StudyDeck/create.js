﻿$(document).ready(function () {
    $(".submitForm").on("click", function () {
        var count = 0;
        var inputs = [];
        var ok = true;
        $('.getCardOnsubmit').each(function () {
            var front = $(this)[0].children[0].value;
            var back = $(this)[0].children[1].value;
            if (front && back && /\S/.test(front) && /\S/.test(back)) {
                inputs.push('<input type="hidden" name="Cards[' + count + '].Front" value="' + front + '" /> ');
                inputs.push('<input type="hidden" name="Cards[' + count + '].Back" value="' + back + '" /> ');
                count++;
            }
            else {
                ok = false;
                toastr.warning("É necessário preencher todo os cards.");
                return false;
            }
        });
        if (ok) {
            for (var i = 0; i < inputs.length; i++) {
                $(this).append(inputs[i]);
            }
            $("#formSubmit").submit();
        }
    });

    $(".card-item-add").on("click", function () {
        var html = '<div class="card-container"><div class="card-item" ><div class="card-item-container">' +
            '<h5>Cartão</h5>' +
            '<div class="card-item-body getCardOnsubmit">' +
            '<textarea placeholder="Frente" class="imputFront"></textarea>' +
            '<textarea placeholder="Verso" class="imputBack"></textarea>' +
            '</div>' +
            '<footer><div><button type="button" class="svg-img remove-card"></button></div></footer>' +
            '</div></div ></div >';
        $('#cards').append(html);
    });

    $("#cards").on("click", ".remove-card", function () {
        $(this)[0].offsetParent.parentElement.parentElement.remove();
    });
});