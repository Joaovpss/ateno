﻿$(document).ready(function () {
    $(function () {
        $("main").on("click", ".addCards", function () {
            $("#modalAddCards").load("/StudyDeck/AddCards?studyDeckId=" + $(this).attr("data-id"), function () {
                $("#modalAddCards").modal('show');
            })
        });
    });

    $(function () {
        $("main").on("click", ".removeDeck", function () {
            $("#modalConfirm").load("/Room/RemoveStudyDeck?id=" + $(this).attr("data-id"), function () {
                $("#modalConfirm").modal('show');
            })
        });
    });

    $(function () {
        $("main").on("click", ".deckInfo", function () {
            $("#modalInfo").load("/Room/DeckInfo?deckId=" + $(this).attr("data-id"), function () {
                $("#modalInfo").modal('show');
            })
        });
    });

    $(function () {
        $(".title").on("click", ".room-code", function () {
            var el = document.getElementById("copyText");
            var range = document.createRange();
            range.selectNodeContents(el);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
            document.execCommand('copy');
            window.getSelection().removeAllRanges();
        });
    });

    var table = $('#myTable').DataTable({
        "dom": "<'row'<'col-sm-12 col-md-6'f><'col-sm-12 col-md-6'i>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>",
        responsive: true,
        orderCellsTop: true,
        fixedHeader: true,
        "ordering": false,
        "oLanguage": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});