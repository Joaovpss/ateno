﻿$(document).ready(function () {
    $(function () {
        $(".header-page").on("click", ".addUser", function () {
            $("#modalAddUser").modal('show');
        });
    });

    $(function () {
        $("tbody").on("click", ".remove-user", function () {
            $("#modalConfirm").load("/Room/RemoveUser?roomId=" + $(this).attr("data-roomId") + "&email=" + $(this).attr("data-email"), function () {
                $("#modalConfirm").modal('show');
            })
        });
    });

    var table = $('#myTable').DataTable({
        "dom": "<'row'<'col-sm-12 col-md-6'f><'col-sm-12 col-md-6'i>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>",
        responsive: true,
        orderCellsTop: true,
        fixedHeader: true,
        "oLanguage": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});