﻿namespace Ateno.Application.DTOs
{
    public class ResponseDTO
    {
        public bool Success { get; set; }
        public int Value { get; set; }
        public string ValueString { get; set; }
        public string Message { get; set; }
    }
}
